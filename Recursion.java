public class Recursion{
    public static void main(String[] args){
        int[] arr = {10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40};

        System.out.println(recursiveCount(arr, 10));
    }

    public static int recursiveCount(int[] numbers, int n){
        //makes sure it stays in bounds
        if(n < numbers.length){
            if (numbers[n] > 20 && n % 2 == 1){
                //valid
                return recursiveCount(numbers, n + 1) + 1;
            }
            //invalid
            return recursiveCount(numbers, n + 1);
        }

        //array ends
        return 0;
    }
}