//Luke Weaver 1937361

package game;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class BtnAction implements EventHandler<ActionEvent>
{
	private TextField input;
	private String roll;
	
	public BtnAction(TextField vInput, String vRoll)
	{
		this.input = vInput;
		this.roll = vRoll;
	}
	
	@Override
	public void handle(ActionEvent e)
	{
		this.input.setText(this.roll);
	}
}
