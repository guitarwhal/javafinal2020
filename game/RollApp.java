//Luke Weaver 1937361

package game;

import javafx.application.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.*;

public class RollApp extends Application{
	public void start(Stage stage){
		Group root = new Group();
		
		VBox game = new VBox();
		
		HBox buttons = new HBox();
		VBox info = new VBox();
		HBox bet = new HBox();
		
		Text moneyLabel = new Text("Money: ");
		Text moneyText = new Text("250");
		Text rollLabel = new Text("Number rolled:");
		Text rollText = new Text("0");
		Text betLabel = new Text("Bet:");
		TextField betText = new TextField("0");
		TextField result = new TextField("");
		
		info.getChildren().add(moneyLabel);
		info.getChildren().add(moneyText);
		info.getChildren().add(rollLabel);
		info.getChildren().add(rollText);
		info.getChildren().add(betLabel);
		info.getChildren().add(betText);
		
		Button evenBtn = new Button("Even");
		Button oddBtn = new Button("Odd");
		TextField betRoll = new TextField("");
		betRoll.setEditable(false);
		
		bet.getChildren().add(evenBtn);
		bet.getChildren().add(oddBtn);
		bet.getChildren().add(betRoll);
		
		BtnAction action1 = new BtnAction(betRoll, evenBtn.getText());
		evenBtn.setOnAction(action1);
		BtnAction action2 = new BtnAction(betRoll, oddBtn.getText());
		oddBtn.setOnAction(action2);
		
		Button roll = new Button("Roll!");
		
		SubmitAction submit = new SubmitAction(rollText, moneyText, result, betText);
		roll.setOnAction(submit);
		
		game.getChildren().add(info);
		game.getChildren().add(bet);
		game.getChildren().add(roll);
		game.getChildren().add(result);
		
		root.getChildren().add(game);
		
		Scene scene = new Scene(root, 800, 600);
		scene.setFill(Color.WHITE);
		
		stage.setTitle("Roll the Dice!");
		stage.setScene(scene);
		stage.show();
	}
	
	public static void main(String[] args){
		Application.launch(args);
	}
}