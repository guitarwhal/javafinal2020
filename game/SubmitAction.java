//Luke Weaver 1937361

package game;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class SubmitAction implements EventHandler<ActionEvent>
{
	private Text rollText;
	private int roll;
	private Text moneyText;
	private int money;
	private TextField betField;
	private int moneyBet;
	private TextField resultText;
	private boolean even;
	private String result;
	
	public SubmitAction(Text rollText, Text moneyText, TextField resultText, TextField betField)
	{
		this.rollText = rollText;
		this.roll = Integer.parseInt(this.rollText.getText());
		this.moneyText = moneyText;
		this.money = Integer.parseInt(this.moneyText.getText());
		this.resultText = resultText;
		this.betField = betField;
		this.moneyBet = Integer.parseInt(this.betField.getText());
		
		if(this.roll % 2 == 0) {
			this.even = true;
		}else {
			this.even = false;
		}
		
	}
	
	@Override
	public void handle(ActionEvent e)
	{
		this.roll = dice.roll();
		this.result = dice.result(this.even, this.roll, this.money);
		
		this.rollText.setText("" + this.roll);
		this.moneyText.setText("" + dice.moneyChange(this.result, this.moneyBet, this.money));
		this.resultText.setText(this.result);
	}
}
