//Luke Weaver 1937361

package game;

import java.util.Random;

public class dice {
	//will return the rolled die as a String
	public static int roll() {
		Random randGen = new Random();
		
		return randGen.nextInt(6) + 1;
	}
	
	public static String result(boolean even, int roll, int money) {
		String result = "";
		
		if(even && roll % 2 == 0) {
			return "win";
		}else if(!even && roll % 2 == 1) {
			return "win";
		}
		
		return "lose";
	}
	
	public static int moneyChange(String result, int bet, int money) {
		if(result.equals("win")) {
			money += bet;
		}else {
			money -= bet;
		}
		return money;
	}
}
